<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    public function testUserList() {

		 $this->json('GET', '/user')
         ->seeJson([
             'result' => 1,
         ]);
    }

    public function testUserListExact() {

		 $this->json('GET', '/user')
         ->seeJsonEquals([
         	'result' => 1,
			'users' => [
				[
					'email' => 'email@gmail.com',
					'password' => 'password'
				],
				[
					'email' => 'email@gmail.com',
					'password' => 'password'
				]
			],
         ]);
    }

    public function testUserCreate() {

    	$this->json('POST', '/user', [
    		'name' => 'Test Name',
    		'email' => 'email@gmail.com',
    		'password' => 'password',
		])->seeJson([
			'result' => 1,
		]);
    }

    public function testUserCreateForm() {

    	$this->visit('/user/create')
			->type('name@gmail.com', 'email')
			->type('password123', 'password')
			->press('Create')
			->seePageIs('/dashboard');
    }

   //  public function testUserCreateFormError() {

   //  	$this->visit('/user/create')
			// // ->type('name@gmail.com', 'email')
			// ->type('password123', 'password')
			// ->press('Create')
			// ->seePageIs('/dashboard');
   //  }
}
