<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller {

	public function index() {

		return response()->json([
			'result' => 1,
			'users' => [
				[
					'email' => "email@gmail.com",
					'password' => "password"
				],
				[
					'email' => "email@gmail.com",
					'password' => "password"
				]
			],
		]);
	}

	public function store(Request $request) {

		$input = $request->all();

		if(isset($input['name'])) {
			return response()->json([
				'result' => 1,
				'user' => $input,
			]);
		}
	}

	public function create(Request $request) {

		return view('users/create');
	}

	public function createNew(Request $request) {

		$input = $request->all();

		if($input['email'] != '') {
			return redirect('dashboard');
		} else {
			return response()->json([
				'result' => 0
			]);
		}
	}
}