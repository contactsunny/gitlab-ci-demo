![Build Status](https://gitlab.com/contactsunny/gitlab-ci-demo/badges/master/build.svg)

## Setup

Clone the repo and ```cd``` into it.

**Install Composer**

[Method 1](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)

Method 2:
```sh
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```

**Install dependencies**

```sh
composer install
```

**Run the app in development**

```
php artisan serve
```

**Run the test cases**

To run all test cases in the ```/tests``` directory, use the command:
```sh
./vendor/bit/phpunit
```

To run a specific test case class, use the command:
```sh
./vendor/bit/phpunit <ClassNameTest.php>
```

[PHPUnit Documentation](https://phpunit.de/)

[Laravel Testing Documentation](https://laravel.com/docs/5.2/testing)